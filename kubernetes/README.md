Install:


kubectl cli -> https://kubernetes.io/docs/tasks/tools/install-kubectl/


# apply config to kubernetes
kubectl apply -f deployment.yml

# get list of deployments
kubectl get deployments

# get list of pods
kubectl get pods

# show pod log (one pod)
kubectl logs -f <POD_ID>
# show logs, all pods that match selector
kubectl logs -f -l=app=test


# get pods by selector ( app=test )
kubectl get pods -l=app=test

# apply service
kubectl apply -f service.yml
# get services, wait unit EXTERNAL-IP is visible and open that in browser
kubectl get services

# delete stuff
kubectl delete -f service.yml

In every lecture

In start of each lecture configure kubectl

download cluster config from digitalocean
place content of that to ~/.kube/config